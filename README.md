kpVilenjak - Scrape items from kupujemprodajem.com
============
*Scrape items from kupujemprodajem.com... format data print data and plays around with data...*

Requirements :

  * beautifulsoup4
  * lxml
  * requests

run :
```bash
pip install -r requirements.txt
```
How to use it :
============
For help you can try : 
```bash
python3.6 main.py -h
```

Optional arguments:
============
```
  -h, --help         show this help message and exit
  -s , --searchkey   Seach for specific keywords parsed.
  -f , --filter      Filters specific keys like used for prints xmls and returns || name, desc, link, ad-id, price, created and geoloc.
  -u , --url         Search for items on specific url.
  -o , --overproxy   Set proxy. -o ip:port.
  -x, --xml          Write data to xml file.
  -p, --printdata    Print the data.
  -d, --data         Return raw data.
```

Available Filters : -f , --filter
============
  * name
  * desc
  * link
  * ad-id
  * price
  * created
  * geoloc

Examples:
```bash
# Search specific keywords -s 'foo bar' -p for printing results
python3 main.py -s 'amd ryzen 7' -p
# Search specific keywords filter output for name and price generate xmls and print results
python3 main.py -s 'amd fx 8350' -f 'name price' -x -p 
# Read all ads from specific link (must be svi-oglasi) and generate xmls
python3 main.py -u "https://www.kupujemprodajem.com/68622-1-pc-world-svi-oglasi.htm" -x
# Read all ads from specific link (must be svi-oglasi), print results and dumps all data (check the code for more info)
python3 main.py -u "https://www.kupujemprodajem.com/68622-1-pc-world-svi-oglasi.htm" -p -d
```
Note:
============
Build virtualenv and then use it.
```bash
# if u have it u can skip this one
pip3 install virtualenv

# make directory for test
mkdir virtenvKp

# create virtualenv
virtualenv virtenv/spider

# activate virtualenv
cd virtenvKp/spider/bin/
source activate

# have fun
```
