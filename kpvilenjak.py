#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
"""
File:       kpvilenjak.py
Author:     @r4ven
Date:       09/24/2019
Desc:       Script that scrape kupujemprodajem.com for items
Version:    1.0
Python:     3.6
"""
# TODO: create ability that checks for new items in background, notify if founds something new for specific keywords/page
# TODO: make it run in docker
# TODO: make it use random proxy for every page

import xml.etree.ElementTree as etree
import lxml.etree
from urllib.parse import urlparse
from bs4 import BeautifulSoup

import requests
import time
import sys

class kpVilenjak(object):

    def __init__(self, dataFilter=None, command=None, search=None, url=None, overproxy=None):
        self.domain = 'https://www.kupujemprodajem.com'
        self.dataFilter = dataFilter
        self.command = command
        self.search = search
        self.url = url

        self.nextpage = None
        self.everything = []
        self.overproxy = overproxy
        
        self.soup = self.__getSoup()
        self.content = self.__formatData(self.soup)
        self.filteredContent = self.__filterFormatedData(self.content)
        
    # get soup from site
    def __getSoup(self, page=1):
        # set url based on command
        def setUrl(command):
            if command == 'search':
                url = '{domain}/search.php?action=list&data[keywords]={search}&data[list_type]=search&data[page]={page}'.format(
                    domain = str(self.domain),
                    search = str(self.__fixSearch(self.search)),
                    page = str(page) )

            elif command == 'allads':
                if not self.url:
                    print('i need url here baby')
                    sys.exit()

                if self.nextpage:
                    url = self.nextpage
                else:
                    url = self.url
            else:
                url = None

            return url

        # request data from server 
        def requester(url):
            if not url:
                print('url is none')
                sys.exit()

            overproxy = self.overproxy
            testurl = 'https://ifconfig.me/ip'
            headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36' }
            proxies = {'http':'http://{}'.format(overproxy),
                       'https':'https://{}'.format(overproxy) }

            # if u go over proxy 
            if overproxy:
                # get real ip
                iptestreq = requests.get(testurl)
                proxyiptest = requests.get(testurl, proxies=proxies)
                # test if proxy works
                # NOTE: need more tests here...
                if iptestreq.text != proxyiptest.text:
                    rawdata = requests.get(url, headers=headers, proxies=proxies, allow_redirects=False).text
                    soup = BeautifulSoup(rawdata, 'lxml')
                    self.nextpage = self.__nextPage(soup)
                    print('org ip:{}\nnew ip:{}'.format(iptestreq.text, proxyiptest.text))
                    return soup
                else:
                    print('proxy didnt work correcrly')
                    print('org ip:{}\nnew ip:{}'.format(iptestreq.text, proxyiptest.text))
                    sys.exit()

            rawdata = requests.get(url, headers=headers, proxies=None, allow_redirects=False).text
            soup = BeautifulSoup(rawdata, 'lxml')

            # set next page if exists
            self.nextpage = self.__nextPage(soup)
            return soup
            
        if not self.command:
            print('i need a command baby')
            sys.exit()

        soup = requester(setUrl(self.command))
        self.nextpage = self.__nextPage(soup)
        return soup

    # If there is ' ' space in the keyword replace it with '%20'
    def __fixSearch(self, search):
        if search:
            if ' ' in search:
                search = search.replace(' ', '%20')
            return search
        return None

    # returns pages count
    def __getPageCount(self):
        pagecountSoup = self.soup.find('div', attrs={'class': 'pageBarHolder'})
        # Check how many pages is there
        if pagecountSoup == None:
            return 1
        else:
            pagecountli = pagecountSoup.find_all('li')[-2]
        return pagecountli.find('a').text

    # gets next page link if exists
    def __nextPage(self, soup):
        nextPageSoup = soup.find('div', attrs={'class': 'pageBarHolder'})
        if nextPageSoup == None:
            return None
        else:
            nextpageli = nextPageSoup.find_all('li')[-1]
            nextpagelink = nextpageli.find('a')

            if nextpagelink == None:
                return None
            else:
                link = nextpagelink.get('href')
                return '{}/{}'.format(self.domain, link)

    # read filters and splits them to list
    def __filterSpliter(self):
        if not self.dataFilter:
            return None
        commandList = self.dataFilter.split()
        return commandList

    # sorts a soup and formats it to list of dicts where 
    # elements of list are items and items are key/val pairs of data
    # [ {'first-item-key': 'first-item-val', ...},
    #   {'second-item-key': '...'}, ... ]
    def __formatData(self, soup):
        content = []
        # Skip first iteration // cus it returns garbage
        iters = iter(soup.find_all('div', attrs={'class': 'item'}))
        next(iters)

        # Gets all items
        for divs in iters:
            itemData = {}

            for name in divs.find_all('section', attrs={'class': 'nameSec'}):
                # Gets item name
                adname = name.find('a', attrs={'class': 'adName'})
                itemData['name'] = adname.get_text(strip=True)

                # Gets item description
                addes = name.find('div', attrs={'class': 'adDescription'})
                itemData['desc'] = addes.get_text(strip=True)

                # Gets item link
                link = name.find('a', attrs={'class': 'adName'})
                itemData['link'] = '{}{}'.format(self.domain if not self.url else '', link.get('href'))

            # Gets item id
            adid = divs.find('div', attrs={'class': 'ad-options'})['ad-id']
            itemData['ad-id'] = adid

            # Gets item price
            price = divs.find('section', attrs={'class': 'priceSec'})
            itemData['price'] = price.get_text(strip=True)

            # Gets meta data like / view count , created...
            for meta in divs.find_all('section', attrs={'class': 'viewsSec'}):
                # Gets view count
                seens = meta.find('div', attrs={'class': 'view-count'})
                itemData['seens'] = seens.get_text(strip=True)

                # Gets item created date
                created = meta.find('div', attrs={'class': 'date-posted'})
                itemData['created'] = created.get_text(strip=True)

            # Gets item Location
            geoloc = divs.find('section', attrs={'class': 'locationSec'})
            itemData['geoloc'] = geoloc.get_text(strip=True)

            # Append item results to content list
            content.append(itemData)

        return content

    # set new content if it exists
    def __newContent(self, page=1, filtered=False):
        if filtered:
            soup = self.__getSoup(page)
            formatedData = self.__formatData(soup)
            data = self.__filterFormatedData(formatedData)
            self.filteredContent = data
            self.everything.append(data)
        else:
            soup = self.__getSoup(page)
            data = self.__formatData(soup)
            self.content = data
            self.everything.append(data)

    # filter data if there is command
    def __filterFormatedData(self, content):
        # if there is no command parsed return None
        if not self.dataFilter:
            return None

        filterdContent = []
        filters = self.__filterSpliter()
        # iterate over list // el of list is a item
        for item in content:
            itemData = {}
            # iterate over item dicts
            for key, value in item.items():
                for com in filters:
                    if key in com:
                        itemData[key] = value

            # append filtered item to filterdContent
            filterdContent.append(itemData)
        return filterdContent

    # print results
    def printContent(self, page=1):
        # print data // if filter is True print filtered data
        def printIt(page=1, data=None):
            pageCount = self.__getPageCount()
            print('{} page-{} {}'.format('*'*25, page, '*'*25))
            # iterate over data
            for item in data:
                for key, value in item.items():
                    printdata = '{}:{}'.format(key, value)
                    print(printdata)       
                print('-' * len(printdata))
            # ¡Viva la Recursión!
            # repeat func and increment page if there is more pages
            if int(page) != int(pageCount):
                page += 1
                if self.dataFilter:
                    self.__newContent(page=page, filtered=True)
                    return printIt(page=page, data=self.filteredContent)
                else:
                    self.__newContent(page=page)
                    return printIt(page=page, data=self.content)
            return 1

        # NOTE:needs more work...
        if self.dataFilter:
            self.__newContent(page=page, filtered=True)
            return printIt(data=self.filteredContent)
        else:
            self.__newContent(page=page)
            return printIt(data=self.content)

    # creates xml files
    # if there is more pages it generates foo-(npage).xml s
    def generateXml(self, page=1):
        # generate xml function
        def genXml(page=1, data=None, filtname=False):
            pageCount = self.__getPageCount()
            root = etree.Element('root')
            # iterate over items // idd is used for labling xml item
            for iid, item in enumerate(data):
                doc = etree.SubElement(root, 'item-{}'.format(iid+1))
                for key, value in item.items():
                    etree.SubElement(doc, key).text = value
            # creating xml 
            tree = etree.ElementTree(root)
            tree.write("kpVilenjak-page-{}{}{}.xml".format(
                page,
                '-' + self.search.replace(' ','-') if self.search else '',
                '-filtered' if filtname else '', ), encoding='utf8')
            # repeat func and increment page if there is more pages
            if int(page) != int(pageCount):
                page += 1
                if self.dataFilter:
                    self.__newContent(page=page, filtered=True)
                    return genXml(page=page, data=self.filteredContent, filtname=True)
                else:
                    self.__newContent(page=page)
                    return genXml(page=page, data=self.content )
            return 1
        # start generating xml
        if self.dataFilter:
            self.__newContent(page=page, filtered=True)
            return genXml(data=self.filteredContent, filtname=True)
        else:
            self.__newContent(page=page)
            return genXml(data=self.content)

    # read xml and return data from it in same format as self.content
    # NOTE: need this one for comparing new results
    def xml2data(self, file):
        parser = lxml.etree.XMLParser(recover=True)
        tree = lxml.etree.parse(file, parser=parser)
        root = tree.getroot()

        itemList = []
        for items in root:
            itemsDict = {}
            for item in items:
                itemsDict[item.tag] = item.text

            itemList.append(itemsDict)
        return itemList

    # return content returns evrithins in list of lists of dicts
    def returnData(self):
        return self.everything

