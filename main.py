#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
from kpvilenjak import kpVilenjak
import argparse
import sys

def main():
    # banner
    banner=(r''' .  *  . *  .  ,  * .  -   * , .  -    ,     *    ,   .   *   ,
 _    ,    *        _ _   ,  *      _   .   _       *    __
| | ___ __   /\   /(_) | ___ _ __  (_) __ _| | __     | /  \ |  jgs
| |/ / '_ \  \ \ / / | |/ _ \ '_ \ | |/ _` | |/ /    \_\\  //_/
|   <| |_) |  \ V /| | |  __/ | | || | (_| |   <  ,   .'/()\'.
|_|\_\ .__/    \_/ |_|_|\___|_| |_|/ |\__,_|_|\_\      \\  //
 .   |_|   -    ,     *    ,   . |__/ . *  -  .  ,  * .  * .
''')

    # print loop
    def printLoop(data):   
        for item in data:
            print()
            for key, val in item.items():
                print(key, val)

    # just for reading data
    # FIXME: dont look at this one plz
    def formatNprint(data): 
        try:
            if type(data) == list:
                for el in data:
                    printLoop(el)
            else:
                printLoop(el)

        except AttributeError:
            printLoop(data)

    parser = argparse.ArgumentParser(description="Scrape items from kupujemprodajem.com... format the data prints the data and plays around with data :)",
    epilog='Example of use : (1) python3 main.py -s "amd ryzen 7" -p -x -f "name desc price" \
        || Searchs for "amd ryzen 7", filterit for name description and price, prints results and generates xml.\
        (2) python3 main.py -u "https://www.kupujemprodajem.com/68622-1-pc-world-svi-oglasi.htm" -x \
        || Reads url (all ads from user) and generates xml s')

    parser.add_argument("-s", "--searchkey", type=str, metavar="", default=None,
    help="Seach for specific keywords parsed.")

    parser.add_argument("-f", "--filter", type=str, metavar="", default=None,
    help="Filters specific keys like used for prints xmls and returns || name, desc, link, ad-id, price, created and geoloc.")

    parser.add_argument("-u", "--url", type=str, metavar="", default=None,
    help="Search for items on specific url.")

    parser.add_argument("-o", "--overproxy", type=str, metavar="", default=None,
    help="Set proxy. -o ip:port.")

    parser.add_argument("-x", "--xml", action='store_true',
    help="Write data to xml file.")

    parser.add_argument("-p", "--printdata", action='store_true',
    help="Print the data.")

    parser.add_argument("-d", "--data", action='store_true',
    help="Return raw data.")

    args = parser.parse_args()

    if len(sys.argv) == 1:
        print(banner)
        parser.print_help(sys.stderr)
        sys.exit()

    searchkey = args.searchkey
    url = args.url
    xmlok = args.xml
    printdata = args.printdata
    overproxy = args.overproxy
    filters = args.filter
    returndata = args.data

    if url and searchkey:
        print('you cant set search keys and url')
        sys.exit()

    if searchkey:
        kp = kpVilenjak(command='search', dataFilter=filters, search=searchkey ,overproxy=overproxy)
        
    if url:
        kp = kpVilenjak(command='allads', dataFilter=filters, url=url ,overproxy=overproxy)
        
    if searchkey or url:
        if xmlok:
            kp.generateXml()

        if printdata:
            kp.printContent()
            
        if returndata:
            data = kp.returnData()
            print(data)
            print('have fun :D') # try formatNprint(data)
    else:
        print('try passing -u (for url) or -s (for search keys)')


if __name__ == '__main__':
    main()




